from __future__ import unicode_literals

VERIFY_REQUESTS = True

SITE = 'http://proxy.docker:8004/'
LOCAL_SITE = 'http://localhost:8006/'

CATALOG = 'http://apis.docker:8080/DSProductCatalog'
INVENTORY = 'http://apis.docker:8080/DSProductInventory'
ORDERING = 'http://apis.docker:8080/DSProductOrdering'
BILLING = 'http://apis.docker:8080/DSBillingManagement'
USAGE = 'http://apis.docker:8080/DSUsageManagement'

RSS = 'http://rss.docker:8080/DSRevenueSharing'

# Keyrock/Keystone settings
KEYSTONE_PROTOCOL = ''
KEYSTONE_HOST = ''

#Use Keyrock7 port for both e.g., 3000
KEYROCK_PORT = '3000'
KEYSTONE_PORT = '3000'

KEYSTONE_USER = ''
KEYSTONE_PWD = ''
ADMIN_DOMAIN = ''


#APP SETTINGS (e.g., Orion context broker)
APP_CLIENT_ID = ''
#PEP Proxy Wilma endpoint + /v2/entities
APP_URL = 'http://<pepwilma_ip_goes_here>:7000/v2/entities/'


AUTHORIZE_SERVICE = 'http://proxy.docker:8004/authorizeService/token'
